import com.abhi.orient.model.Node;
import com.orientechnologies.orient.core.db.ODatabaseSession;
import com.orientechnologies.orient.core.db.ODatabaseType;
import com.orientechnologies.orient.core.db.OrientDB;
import com.orientechnologies.orient.core.db.OrientDBConfig;
import com.orientechnologies.orient.core.record.OEdge;
import com.orientechnologies.orient.core.record.OVertex;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

/**
 * This main class loads data into OrientDB along the format of Task Tree
 */
public class OrientTestMain2 {
    public static void main(String arr[]) {
        SimpleDateFormat format = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");
        Long millis = System.currentTimeMillis();
        Date date = new Date(millis);
        System.out.println("Test begins at: " + format.format(date));

        OrientDB orient = new OrientDB("remote:localhost","root","root",OrientDBConfig.defaultConfig());
        orient.create("ClientRequest",ODatabaseType.PLOCAL);
        System.out.println("Database created at: "+format.format(date));

        ODatabaseSession db = orient.open("ClientRequest", "root", "root");

        if (db.getClass("Node") == null) {
            db.createVertexClass("Node");
        }
        if (db.getClass("TaskOf") == null) {
            db.createEdgeClass("TaskOf");
        }

        Node node = new Node("CSR", "AY", new Date("12/31/2018"));
        node.addContext(Node.ContextType.CLIENT, 122)
            .addAttribute("RandomRefId", 145);

        OVertex vertex1 = createNode(db, node);

        Integer[] contractArray = {123444, 123555};
        Node node2 = new Node("KYC", "DT", new Date("12/31/2018"));
        node2.addContext(Node.ContextType.CLIENT, 123)
            .addContext(Node.ContextType.PORTFOLIO, 154)
            .addAttribute("RandomRefId", 145)
            .addAttribute("ContractList", Arrays.asList(contractArray));

        OVertex vertex2 = createNode(db, node2);

        OEdge edge1 = vertex1.addEdge(vertex2, "TaskOf");
        edge1.isLightweight();
        edge1.save();

        Integer[] portfolioArray = {1411, 1511};
        Node node3 = new Node("PRM", "WJ", new Date("12/31/2018"));
        node3.addContext(Node.ContextType.PORTFOLIO, 153)
                .addContext(Node.ContextType.PORTFOLIO, 154)
                .addAttribute("RandomRefId", 145)
                .addAttribute("DocumentList", Arrays.asList(portfolioArray));

        OVertex vertex3 = createNode(db, node3);
        OEdge edge2 = vertex1.addEdge(vertex3, "TaskOf");
        edge2.isLightweight();
        edge2.save();

        db.close();
        orient.close();
    }

    private static OVertex createNode(ODatabaseSession db, Node node) {
        OVertex result = db.newVertex("Node");
        result.setProperty("reqQueue", node.reqQueue);
        result.setProperty("owner", node.owner);
        result.setProperty("dueDate", node.dueDate);
        result.setProperty("attributesMap", node.attributesMap);
        result.setProperty("contextMap", node.contextMap);
        result.save();
        return result;
    }


    /**
     * Query Maps
     *
     * select *,contextMap.keys() from `Node` where contextMap.keys() contains "PORTFOLIO"
     */
}
