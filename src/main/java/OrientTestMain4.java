import com.abhi.orient.model.Attribute;
import com.abhi.orient.model.Context;
import com.abhi.orient.model.Node2;
import com.orientechnologies.orient.core.db.ODatabaseSession;
import com.orientechnologies.orient.core.db.ODatabaseType;
import com.orientechnologies.orient.core.db.OrientDB;
import com.orientechnologies.orient.core.db.OrientDBConfig;
import com.orientechnologies.orient.core.record.OEdge;
import com.orientechnologies.orient.core.record.OVertex;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class OrientTestMain4 {

    public static void main(String arr[]) {
        SimpleDateFormat format = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");
        Long millis = System.currentTimeMillis();
        Date date = new Date(millis);
        System.out.println("Test begins at: " + format.format(date));

        OrientDB orient = new OrientDB("remote:localhost","root","root",OrientDBConfig.defaultConfig());
        orient.create("ClientRequest2",ODatabaseType.PLOCAL);
        System.out.println("Database created at: "+format.format(date));

        ODatabaseSession db = orient.open("ClientRequest2", "root", "root");

        if (db.getClass("Node") == null) {
            db.createVertexClass("Node");
            db.createVertexClass("Attribute");
            db.createVertexClass("Context");
        }
        if (db.getClass("TaskOf") == null) {
            db.createEdgeClass("TaskOf");
            db.createEdgeClass("ContextOf");
            db.createEdgeClass("AttributeOf");
        }

        Node2 node = new Node2("CSR", "AY", new Date("12/31/2018"));
        Context context = new Context(Node2.ContextType.CLIENT.toString(), 122);
        Attribute attribute = new Attribute("RandomRefId", 145);

        OVertex vertex1 = createNode(db, node);
        OVertex vertexC1 = createContext(db, context);
        OVertex vertexA1 = createAttribute(db, attribute);
        vertex1.addEdge(vertexC1, "ContextOf").save();
        vertex1.addEdge(vertexA1, "AttributeOf").save();


        Integer[] contractArray = {123444, 123555};
        Node2 node2 = new Node2("KYC", "DT", new Date("12/31/2018"));
        Context context2 = new Context(Node2.ContextType.CLIENT.toString(), 123);
        Attribute attribute2 = new Attribute("RandomRefId", 145);
        Attribute attribute22 = new Attribute("ContractList", Arrays.asList(contractArray));

        /*node2.addContext(Node.ContextType.CLIENT, 123)
                .addContext(Node.ContextType.PORTFOLIO, 154)
                .addAttribute("RandomRefId", 145)
                .addAttribute("ContractList", Arrays.asList(contractArray));*/

        OVertex vertex2 = createNode(db, node2);
        OVertex vertexC2 = createContext(db, context2);
        OVertex vertexA2 = createAttribute(db, attribute2);
        OVertex vertexA22 = createAttribute(db, attribute22);

        OEdge edge1 = vertex1.addEdge(vertex2, "TaskOf");
        edge1.isLightweight();
        edge1.save();
        vertex2.addEdge(vertexC2, "ContextOf").save();
        vertex2.addEdge(vertexA2, "AttributeOf").save();
        vertex2.addEdge(vertexA22, "AttributeOf").save();

        Integer[] portfolioArray = {1411, 1511};
        Node2 node3 = new Node2("PRM", "WJ", new Date("12/31/2018"));
        Context context3 = new Context(Node2.ContextType.PORTFOLIO.toString(), 153);
        Attribute attribute3 = new Attribute("RandomRefId", 154);
        Attribute attribute32 = new Attribute("PortfolioList", Arrays.asList(portfolioArray));

        /*node3.addContext(Node.ContextType.PORTFOLIO, 153)
                .addContext(Node.ContextType.PORTFOLIO, 154)
                .addAttribute("RandomRefId", 145)
                .addAttribute("DocumentList", Arrays.asList(portfolioArray));*/

        OVertex vertex3 = createNode(db, node3);
        OVertex vertexC3 = createContext(db, context3);
        OVertex vertexA3 = createAttribute(db, attribute3);
        OVertex vertexA32 = createAttribute(db, attribute32);

        OEdge edge2 = vertex1.addEdge(vertex3, "TaskOf");
        edge2.isLightweight();
        edge2.save();
        vertex3.addEdge(vertexC3, "ContextOf").save();
        vertex3.addEdge(vertexA3, "AttributeOf").save();
        vertex3.addEdge(vertexA32, "AttributeOf").save();


        db.close();
        orient.close();
    }

    private static OVertex createNode(ODatabaseSession db, Node2 node) {
        OVertex result = db.newVertex("Node");
        result.setProperty("reqQueue", node.reqQueue);
        result.setProperty("owner", node.owner);
        result.setProperty("dueDate", node.dueDate);
        result.save();
        return result;
    }

    private static OVertex createContext(ODatabaseSession db, Context node) {
        OVertex result = db.newVertex("Context");
        result.setProperty("key", node.getKey());
        result.setProperty("value", node.getValue());
        result.save();
        return result;
    }

    private static OVertex createAttribute(ODatabaseSession db, Attribute node) {
        OVertex result = db.newVertex("Attribute");
        result.setProperty("key", node.getKey());
        result.setProperty("value", node.getValue());
        result.save();
        return result;
    }


    /**
     * Query Maps
     *
     * select *,contextMap.keys() from `Node` where contextMap.keys() contains "PORTFOLIO"
     */
}
