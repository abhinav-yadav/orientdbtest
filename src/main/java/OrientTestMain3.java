import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This main class reads data written using GraphQL
 */
public class OrientTestMain3 {

    public static void main(String arr[]) {
        SimpleDateFormat format = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");
        Long millis = System.currentTimeMillis();
        Date date = new Date(millis);
        System.out.println("Test begins at: " + format.format(date));

        OrientTestGraphQL orientTestGraphQL = new OrientTestGraphQL();
        orientTestGraphQL.printDetails();
    }

}
