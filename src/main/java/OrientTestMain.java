import com.orientechnologies.orient.core.db.ODatabaseSession;
import com.orientechnologies.orient.core.db.ODatabaseType;
import com.orientechnologies.orient.core.db.OrientDB;
import com.orientechnologies.orient.core.db.OrientDBConfig;
import com.orientechnologies.orient.core.record.OEdge;
import com.orientechnologies.orient.core.record.OVertex;
import com.orientechnologies.orient.core.sql.executor.OResultSet;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

/**
 * This main class loads random data into OrientDB
 */
public class OrientTestMain {

    public static void main(String arr[]){
        SimpleDateFormat format = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");
        Long millis = System.currentTimeMillis();
        Date date = new Date(millis);
        System.out.println("Test begins at: "+format.format(date));

        OrientDB orient = new OrientDB("remote:localhost","root","root",OrientDBConfig.defaultConfig());
        createDatabase(orient);

        System.out.println("Database created at: "+format.format(date));
        System.exit(0);
    }

    private static void createDatabase(OrientDB orient){
        orient.create("TestOrient1",ODatabaseType.PLOCAL);
        ODatabaseSession db = orient.open("TestOrient1", "root", "root");

        if (db.getClass("Person") == null) {
            db.createVertexClass("Person");
        }
        if (db.getClass("FriendOf") == null) {
            db.createEdgeClass("FriendOf");
        }

        OVertex alice = createPerson(db, "Alice", "Yadav");
        OVertex bob = createPerson(db, "Bob", "Yadav");
        OVertex jim = createPerson(db, "Jim", "Desai");

        OEdge edge1 = alice.addEdge(bob, "FriendOf");
        edge1.save();
        OEdge edge2 = bob.addEdge(jim, "FriendOf");
        edge2.save();

        executeAQuery(db);

        db.close();
        orient.close();

    }

    private static OVertex createPerson(ODatabaseSession db, String name, String surname) {
        OVertex result = db.newVertex("Person");
        result.setProperty("name", name);
        result.setProperty("surname", surname);
        result.save();
        return result;
    }

    private static void executeAQuery(ODatabaseSession db) {
        String query = "SELECT expand(out('FriendOf').out('FriendOf')) from Person where name = ?";
        OResultSet rs = db.query(query, "Alice");
        rs.stream().forEach(x -> System.out.println("friend: " + x.getProperty("name")));
        rs.close();
    }

    private static OVertex getVertex(ODatabaseSession db, String name) {
        String query = "SELECT * from Person where name = ?";
        Optional<OVertex> vertex = null;
        OResultSet rs = db.query(query, name);
        while(rs.hasNext()){
            vertex = rs.next().getVertex();
            break;
        }
        rs.close();

        return vertex!=null ? vertex.get() : null;
    }
}
