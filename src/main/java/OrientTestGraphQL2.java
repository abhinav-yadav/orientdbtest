import com.abhi.orient.model.Attribute;
import com.abhi.orient.model.Context;
import com.abhi.orient.model.Node2;
import com.orientechnologies.orient.core.db.ODatabaseSession;
import com.orientechnologies.orient.core.db.OrientDB;
import com.orientechnologies.orient.core.db.OrientDBConfig;
import com.orientechnologies.orient.core.record.ODirection;
import com.orientechnologies.orient.core.record.OVertex;
import com.orientechnologies.orient.core.sql.executor.OResultSet;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.*;

import static graphql.schema.idl.RuntimeWiring.newRuntimeWiring;

/**
 * This reads the GraphQL query from json file and queries the existing 'ClientRequest' DB
 * in OrientDB
 *
 * https://www.howtographql.com/basics/2-core-concepts/
 * https://blog.apollographql.com/graphql-explained-5844742f195e
 */
public class OrientTestGraphQL2 {

    public void printDetails(){
        String schema =readFile("schema2.json");
        SchemaParser schemaParser = new SchemaParser();
        TypeDefinitionRegistry typeDefinitionRegistry = schemaParser.parse(schema);

        Map<String, DataFetcher> stringDataFetcherMap = new HashMap();
        stringDataFetcherMap.put("getNode", new CustomDataFetcher());
        stringDataFetcherMap.put("getAttributeList", new CustomAttributeFetcher());
        RuntimeWiring runtimeWiring = newRuntimeWiring()
                .type("Query", builder -> builder.dataFetchers(stringDataFetcherMap))
                .build();

        SchemaGenerator schemaGenerator = new SchemaGenerator();
        GraphQLSchema graphQLSchema = schemaGenerator.makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);

        GraphQL build = GraphQL.newGraphQL(graphQLSchema).build();
        ExecutionResult executionResult = build.execute(readFile("query.json"));

        System.out.println("Any response: "+executionResult.getData() + " Or errors: "+ executionResult.getErrors());

        System.exit(0);
    }

    public class CustomDataFetcher implements DataFetcher{

        @Override
        public Object get(DataFetchingEnvironment dataFetchingEnvironment) throws Exception {
            //System.out.println("DataFetchingEnvironment passed: "+dataFetchingEnvironment);
            List<Node2> node = getNode(dataFetchingEnvironment.getArgument("reqQueue"));
            System.out.println("Node found: "+node);
            return node;
        }

        private List<Node2> getNode(String owner) {
            OrientDB orient = new OrientDB("remote:localhost","root","root",OrientDBConfig.defaultConfig());
            ODatabaseSession db = orient.open("ClientRequest2", "root", "root");

            String query = "SELECT * from Node where reqQueue = ?";
            //System.out.println("Query to run: "+ query+" owner_passed: "+owner);
            OResultSet rs = db.query(query, owner);
            List<Node2> nodeList = new ArrayList<>();
            while(rs.hasNext()){
                Optional<OVertex> vertex = rs.next().getVertex();
                //System.out.println("Vertex returned: "+ vertex.get().getProperty("attributesMap"));
                if(vertex.isPresent()){
                    Node2 node = new Node2(vertex.get().getProperty("reqQueue"),
                            vertex.get().getProperty("owner")
                            , vertex.get().getProperty("dueDate")
                    );
                    Iterable<OVertex> vertexItr = vertex.get()
                            .getVertices(ODirection.OUT, "AttributeOf");

                    for(OVertex attrVertex:vertexItr){
                        System.out.println("Vertex returned: "+ attrVertex.getProperty("key"));

                        node.getAttributeList().add(new Attribute(attrVertex.getProperty("key"),
                                attrVertex.getProperty("value")));
                    }

                    Iterable<OVertex> vertexCItr = vertex.get()
                            .getVertices(ODirection.OUT, "ContextOf");

                    for(OVertex contextVertex:vertexCItr){
                        System.out.println("Vertex returned: "+ contextVertex.getProperty("key"));

                        node.getContextList().add(new Context(contextVertex.getProperty("key"),
                                contextVertex.getProperty("value")));
                    }
                    nodeList.add(node);
                }
            }
            rs.close();

            return nodeList;
        }
    }

    /**
     * ADDED JUST FOR TESTING - Attributes can come back directly from Nodes
     */
    public class CustomAttributeFetcher implements DataFetcher {

        @Override
        public Object get(DataFetchingEnvironment dataFetchingEnvironment) throws Exception {
            System.out.println("DataFetchingEnvironment for ATTRIBUTE passed: " + dataFetchingEnvironment);
            //List<Node> node = dataFetchingEnvironment.getArgument("reqQueue"));
            System.out.println("Queue found: " + dataFetchingEnvironment.getArgument("key"));
            return new HashMap();
        }
    }

    private String readFile(String filename){
        File file = new File("./src/main/resources/"+filename);
        StringBuffer sb = new StringBuffer();
        try {

            BufferedReader br = new BufferedReader(new FileReader(file));
            String st;
            while ((st = br.readLine()) != null){
                //System.out.println(st);
                sb.append(st);
            }

        }catch(Exception e) {
            System.out.println("Exception: "+e);
        }
        return sb.toString();
    }

}
