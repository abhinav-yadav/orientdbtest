package com.abhi.orient.model;

import java.util.Objects;

public class Context {

    String key;
    Object value;

    public Context(String key, Object value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Context context = (Context) o;
        return Objects.equals(key, context.key) &&
                Objects.equals(value, context.value);
    }

    @Override
    public int hashCode() {

        return Objects.hash(key, value);
    }

    public enum ContextType{
        CLIENT,
        DOCUMENT,
        PORTFOLIO,
        CONTRACT
    }
}
