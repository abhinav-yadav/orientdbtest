package com.abhi.orient.model;

import java.util.Objects;

public class Attribute {
    String key;
    Object value;

    public Attribute(String key, Object value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public Object getValue() {
        return value;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Attribute attribute = (Attribute) o;
        return Objects.equals(getKey(), attribute.getKey()) &&
                Objects.equals(getValue(), attribute.getValue());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getKey(), getValue());
    }
}
