package com.abhi.orient.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Node2 {
    public String reqQueue;
    public String owner;
    public Date dueDate;
    public List<Attribute> attributeList = new ArrayList<>();
    public List<Context> contextList = new ArrayList<>();

    public Node2(String reqQueue, String owner, Date dueDate){
        this.reqQueue = reqQueue;
        this.owner = owner;
        this.dueDate = dueDate;
    }

    public List<Attribute> getAttributeList() {
        return attributeList;
    }

    public void setAttributeList(List<Attribute> attributeList) {
        this.attributeList = attributeList;
    }

    public List<Context> getContextList() {
        return contextList;
    }

    public void setContextList(List<Context> contextList) {
        this.contextList = contextList;
    }

    public void setReqQueue(String reqQueue) {
        this.reqQueue = reqQueue;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public String getReqQueue() {
        return reqQueue;
    }

    public String getOwner() {
        return owner;
    }

    public Date getDueDate() {
        return dueDate;
    }

    @Override
    public String toString() {
        return "Node{" +
                "reqQueue='" + reqQueue + '\'' +
                ", owner='" + owner + '\'' +
                ", dueDate=" + dueDate +
                ", attributesMap=" + attributeList +
                ", contextMap=" + contextList +
                '}';
    }

    public enum ContextType{
        CLIENT,
        DOCUMENT,
        PORTFOLIO,
        CONTRACT
    }
}
