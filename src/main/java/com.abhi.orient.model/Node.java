package com.abhi.orient.model;

import java.util.*;

/***
 Model for every node in the graph

 **/
public class Node{
    public String reqQueue;
    public String owner;
    public Date dueDate;
    public Map<String, Object> attributesMap;
    public Map<ContextType, List<Object>> contextMap;

    public Node(){
    }

    public Node(String reqQueue, String owner, Date dueDate){
        attributesMap = new HashMap<>();
        contextMap = new HashMap<>();
        this.reqQueue = reqQueue;
        this.owner = owner;
        this.dueDate = dueDate;
    }

    public Node addContext(ContextType contextType, Object contextVal){
        List<Object> contexts = contextMap.get(contextType);
        if(contexts ==null){
            contexts = new ArrayList<>();
        }
        contexts.add(contextVal);
        contextMap.put(contextType, contexts);
        return this;
    }

    public Node addAttribute(String key, Object value){
        attributesMap.put(key, value);
        return this;
    }

    public void setReqQueue(String reqQueue) {
        this.reqQueue = reqQueue;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public void setAttributesMap(Map<String, Object> attributesMap) {
        this.attributesMap = attributesMap;
    }

    public void setContextMap(Map<ContextType, List<Object>> contextMap) {
        this.contextMap = contextMap;
    }

    public String getReqQueue() {
        return reqQueue;
    }

    public String getOwner() {
        return owner;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public Map<String, Object> getAttributesMap() {
        return attributesMap;
    }

    public Map<ContextType, List<Object>> getContextMap() {
        return contextMap;
    }

    @Override
    public String toString() {
        return "Node{" +
                "reqQueue='" + reqQueue + '\'' +
                ", owner='" + owner + '\'' +
                ", dueDate=" + dueDate +
                ", attributesMap=" + attributesMap +
                ", contextMap=" + contextMap +
                '}';
    }

    public enum ContextType{
        CLIENT,
        DOCUMENT,
        PORTFOLIO,
        CONTRACT
    }

}